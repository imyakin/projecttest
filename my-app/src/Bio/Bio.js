import React from 'react';

const Bio = (props) => {
    return (
        <div className="bio">
            <h1>{props.name} {props.surname}</h1>
            <p><span>Name:</span> {props.name}</p>
            <p><span>Surname:</span> {props.surname}</p>
        </div>   
    )
};

export default Bio;

