import React from 'react';
import Bio from '../Bio/Bio';

const Header = () => {
    return (
        <Bio 
        name="Alex"
        surname="Myakin"
        />
    )
};

export default Header;