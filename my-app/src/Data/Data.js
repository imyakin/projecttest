import React from 'react';

const Data = (props) => {
    return (
        <div className="data">
            <h3>{props.day}.{props.month}.{props.year}</h3>
        </div>
    )
};

export default Data;